/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author allanmual
 */
public class MiPanel extends JPanel implements KeyListener {

    private Snake culebra;
    private int ms;

    public MiPanel() {
        culebra = new Snake();
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        ms+=200;
        
        int s=ms/1000;
        System.out.println(s);
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, 800, 600);
        culebra.pintar(g);
        culebra.mover();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //System.out.println("Typed Code: " + e.getKeyCode() + " Char: " + e.getKeyChar());
    }

    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("Pressed Code: " + e.getKeyCode() + " Char: " + e.getKeyChar());
        culebra.cambiarDir(e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //System.out.println("Released Code: " + e.getKeyCode() + " Char: " + e.getKeyChar());
    }

}
