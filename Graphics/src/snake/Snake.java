/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Snake {

    private Bola manzana;
    private LinkedList<Bola> snake;
    private int dir;//1 izq, 2 arriba, 3 der, 4 aba

    public Snake() {
        snake = new LinkedList<>();
        config();
    }

    public void pintar(Graphics g) {
        manzana.pintar(g);
        for (Bola bola : snake) {
            bola.pintar(g);
        }
    }

    private void config() {
        manzana = new Bola(Color.green, 100, 100);
        snake.add(new Bola(Color.red, 400, 300));
        snake.add(new Bola(Color.lightGray, 420, 300));
        snake.add(new Bola(Color.lightGray, 440, 300));

    }

    public void mover() {
        Bola c = snake.getFirst();

        //if (c.getX() == manzana.getX() && c.getY() == manzana.getY()) {
        if (c.getBounds().intersects(manzana.getBounds())) {
            System.out.println("Encima de la mazana");
            manzana.setX(((int) (Math.random() * 39)) * 20);
            manzana.setY(((int) (Math.random() * 29)) * 20);
        } else if (dir > 0) {
            snake.removeLast();
        }
        if (dir == 1) {
            snake.getFirst().setColor(Color.lightGray);
            Bola cab = new Bola(Color.red, snake.getFirst().getX() - 20, snake.getFirst().getY());
            snake.addFirst(cab);
        } else if (dir == 2) {
            snake.getFirst().setColor(Color.lightGray);
            Bola cab = new Bola(Color.red, snake.getFirst().getX(), snake.getFirst().getY() - 20);
            snake.addFirst(cab);
        } else if (dir == 3) {
            snake.getFirst().setColor(Color.lightGray);
            Bola cab = new Bola(Color.red, snake.getFirst().getX() + 20, snake.getFirst().getY());
            snake.addFirst(cab);
        } else if (dir == 4) {
            snake.getFirst().setColor(Color.lightGray);
            Bola cab = new Bola(Color.red, snake.getFirst().getX(), snake.getFirst().getY() + 20);
            snake.addFirst(cab);
        }

    }

    public void cambiarDir(int keyCode) {
        if (keyCode >= 37 && keyCode <= 40) {
            dir = keyCode - 36;
        }
    }

}
