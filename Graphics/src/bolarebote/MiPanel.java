/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bolarebote;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author allanmual
 */
public class MiPanel extends JPanel{
    
    private Bola bolita;

    public MiPanel() {
        bolita = new Bola(Color.BLUE, 375, 275);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g); 
        bolita.pintar(g);
        bolita.mover();
        bolita.rebotar(getWidth(), getHeight());
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }
    
        
    
}
