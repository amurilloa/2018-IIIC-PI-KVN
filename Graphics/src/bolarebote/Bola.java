/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bolarebote;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author allanmual
 */
public class Bola {

    private Color color;
    private int x;
    private int y;
    private int dir;
    private final int VEL = 5;

    public Bola(Color color, int x, int y) {
        this.color = color;
        this.x = x;
        this.y = y;
        dir = 2;

    }

    public void pintar(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, 50, 50);
    }

    public void mover() {
        if (dir == 1) {
            y += VEL;
        } else if (dir == 2) {
            y -= VEL;
        } else if (dir == 3) {
            x += VEL;
        } else if (dir == 4) {
            x -= VEL;
        } else if (dir == 5) {
            x += VEL;
            y += VEL;
        } else if (dir == 6) {
            x -= VEL;
            y -= VEL;
        } else if (dir == 7) {
            x -= VEL;
            y += VEL;
        } else if (dir == 8) {
            x += VEL;
            y -= VEL;
        }
    }

    public void rebotar(int limX, int limY) {
        if (x <= 0) {
            int op = (int) (Math.random() * 3) + 1;
            dir = op == 1 ? 8 : op == 2 ? 3 : 5;
        } else if (y <= 0) {
            int op = (int) (Math.random() * 3) + 1;
            dir = op == 1 ? 7 : op == 2 ? 1 : 5;
        } else if (x >= limX - 50) {
            int op = (int) (Math.random() * 3) + 1;
            dir = op == 1 ? 6 : op == 2 ? 4 : 7;
            
        } else if (y >= limY - 50) {
            int op = (int) (Math.random() * 3) + 1;
            dir = op == 1 ? 6 : op == 2 ? 2 : 8;
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

}
