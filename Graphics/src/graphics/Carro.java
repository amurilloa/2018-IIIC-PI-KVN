/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Carro {

    private int placa;
    private boolean encendido;
    private int velocidad;

    //Atributos Grafica
    private int x;
    private int y;
    private Color colPri;
    private Color colSec;

    public Carro(int placa, int x, int y, Color colPri, Color colSec) {
        this.placa = placa;
        this.x = x;
        this.y = y;
        this.colPri = colPri;
        this.colSec = colSec;
    }

    public void pintar(Graphics g) {
        g.setColor(colPri);
        int[] xs = {x + 30, x + 50, x + 80, x + 140, x + 170, x + 220, x + 220, x + 180, x + 30};
        int[] ys = {y + 30, y + 30, y + 0, y + 0, y + 30, y + 35, y + 55, y + 60, y + 60};
        g.fillPolygon(xs, ys, xs.length);

        g.setColor(colSec);
        int[] xsV = {x + 80, x + 137, x + 167, x + 53};
        int[] ysV = {y + 3, y + 3, y + 30, y + 30};
        g.fillPolygon(xsV, ysV, xsV.length);

        g.setColor(Color.black);
        g.fillOval(x + 45, y + 40, 40, 40);
        g.fillOval(x + 150, y + 40, 40, 40);

        if (encendido) {
            g.setColor(Color.lightGray);
            g.fillOval(x + 20, y + 45, 20, 20);
            g.fillOval(x + 10, y + 35, 20, 20);
            g.fillOval(x + 5, y + 45, 20, 20);
            g.setColor(Color.yellow);
            g.fillRect(x + 208, y + 37, 10, 10);
        } else {
            g.setColor(colSec);
            g.fillRect(x + 208, y + 37, 10, 10);

        }
        if (colPri == Color.red) {
            g.setColor(Color.yellow);
            int[] xsT = {x + 130, x + 120, x + 140};
            int[] ysT = {y + 32, y + 52, y + 52};
            g.fillPolygon(xsT, ysT, xsT.length);
            g.setColor(Color.black);
//            g.setFont(new Font("Arial", Font.PLAIN, 4));
            g.drawString(String.valueOf(placa), x + 127, y + 48);

        }

    }

    public int getPlaca() {
        return placa;
    }

    public void setPlaca(int placa) {
        this.placa = placa;
    }

    public boolean isEncendido() {
        return encendido;
    }

    public void setEncendido(boolean encendido) {
        this.encendido = encendido;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColPri() {
        return colPri;
    }

    public void setColPri(Color colPri) {
        this.colPri = colPri;
    }

    public Color getColSec() {
        return colSec;
    }

    public void setColSec(Color colSec) {
        this.colSec = colSec;
    }

}
