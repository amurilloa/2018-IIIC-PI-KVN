/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import javax.swing.JFrame;

/**
 *
 * @author ALLAN
 */ 
public class Graphics {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        //Linl del video
        //http://www.youtube.com/watch?v=zq9a6D7qmP4
        //Crear el formulario
        JFrame frm = new JFrame("DemoGraphics v0.1");
        //Asignar una funcion de cierre al formulario
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Agregar un panel formulario
        frm.add(new MiPanel());
        //Validar la interfaz
        frm.pack();
        //Asignar las dimensiones
        //frm.setSize(800, 1000);
        //Centrar la ventana
        frm.setLocationRelativeTo(null);
        //Visible
        frm.setVisible(true);
        while(true){
            frm.repaint();
            Thread.sleep(40);
        }
    }
    
}
