/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Bola {

    private int x;
    private int y;
    private Color color;
    private int dir;

    public Bola(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
        dir = 1;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void pintar(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, 50, 50);
    }

    public void mover() {
        if (dir == 0) {
            x += 5;
        } else if (dir == 1) {
            y += 5;
        } else if (dir == 2) {
            x -= 5;
        } else if (dir == 3) {
            y -= 5;
        }
    }

    public void rebotar(int limX, int limY) {
        if (x >= limX - 50) {
            dir = 2;
        } else if (x <= 0) {
            dir = 0;
        } else if (y <= 0) {
            dir = 1;
        } else if (y >= limY - 50) {
            dir = 3;
        }
    }
}
