/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cronometro;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Cronometro {

    private int ms;
    private int seg;
    private int min;
    private int hor;
    private boolean corriendo;
    private LinkedList<String> tiempos;

    public Cronometro() {
        tiempos = new LinkedList<>();
    }

    public void incrementarTiempo(int ms) {
        if (corriendo) {
            this.ms += ms;
        }
        int repMs = this.ms;
        hor = repMs / 1000 / 60 / 60;
        repMs = repMs - (hor * 60 * 60 * 1000);
        min = repMs / 1000 / 60;
        repMs = repMs - (min * 60 * 1000);
        seg = repMs / 1000;

    }

    public void dibujarNumero(Graphics g, int num, int x, int y) {
        g.setColor(Color.WHITE);
        if (num >= 0 && num <= 9) {

            if (num != 1 && num != 4) {
                g.fillRect(x, y, 80, 10);
            }

            if (num != 1 && num != 7 && num != 0) {
                g.fillRect(x, y + 80, 80, 10);
            }

            if (num != 1 && num != 7 && num != 4) {

                g.fillRect(x, y + 160, 80, 10);
            }

            if (num != 1 && num != 2 && num != 3 && num != 7) {
                g.fillRect(x, y, 10, 80);
            }

            if (num != 5 && num != 6) {
                g.fillRect(x + 70, y, 10, 80);
            }

            if (num == 2 || num == 6 || num == 8 || num == 0) {
                g.fillRect(x, y + 80, 10, 80);
            }

            if (num != 2) {
                g.fillRect(x + 70, y + 80, 10, 80);
            }
        }
    }

    void pintar(Graphics g) {

        int uh = hor % 10;
        int dh = hor / 10;

        dibujarNumero(g, dh, 100, 100);
        dibujarNumero(g, uh, 200, 100);

        int um = min % 10;
        int dm = min / 10;

        dibujarNumero(g, dm, 320, 100);
        dibujarNumero(g, um, 420, 100);

        int us = seg % 10;
        int ds = seg / 10;

        dibujarNumero(g, ds, 540, 100);
        dibujarNumero(g, us, 640, 100);

        g.setFont(new Font("Arial", Font.PLAIN, 40));
        int y = 350;
        int num = 1;
        for (String tiempo : tiempos) {
            g.drawString(num + " >>> " + tiempo, 450, y);
            y += 40;
            num++;
        }
    }

    public void iniciar() {
        corriendo = true;
    }

    public void detener() {
        corriendo = false;
    }

    public void reiniciar() {
        ms = 0;
        tiempos.clear();
    }

    public void parcial() {
        if (corriendo) {
            String h = String.format("%02d", hor);
            String m = String.format("%02d", min);
            String s = String.format("%02d", seg);
            if(tiempos.size()>=7){
                tiempos.removeFirst();
            }
            tiempos.add(h + ":" + m + ":" + s);
        }
    }

}
