/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cronometro;

import javax.swing.JFrame;

/**
 *
 * @author allanmual
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        JFrame frm = new JFrame("Cronometro UTN v0.1");
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.add(new MiPanel());
        frm.pack();
        frm.setLocationRelativeTo(null);
        frm.setVisible(true);
        while(true){
            frm.repaint();
            Thread.sleep(200);
        }
        
    }
}
