/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriodos;

/**
 *
 * @author ALLAN
 */
public class PintorUTN {

    private String cliente;
    private double tParedes;
    private double tVentanas;

    final int COSTO_X_HORA = 30;
    final int MIN_X_M2 = 10;

    public PintorUTN(String cliente) {
        this.cliente = cliente;
    }

    /**
     * Crea una pared rectangular, calcula el area y lo suma al total de paredes
     *
     * @param ancho double ancho de la pared
     * @param largo double largo de la pared
     */
    public void agregarPared(double ancho, double largo) {
        Rectangulo pared = new Rectangulo(largo, ancho);
        tParedes += pared.calcularArea();
    }

    /**
     * Crea una ventana rectangular, calcula el area y lo suma al total de
     * ventanas
     *
     * @param ancho double ancho de la ventana
     * @param largo double largo de la ventana
     */
    public void agregarVentana(double ancho, double largo) {
        Rectangulo v = new Rectangulo(largo, ancho);
        tVentanas += v.calcularArea();
    }

    /**
     * Crea una ventana circular, calcula el area y lo suma al total de ventanas
     *
     * @param ancho double ancho de la ventana
     */
    public void agregarVentana(double ancho) {
        Circunferencia v = new Circunferencia(ancho / 2);
        tVentanas += v.calcularArea();
    }

    /**
     * Calcula las variables de la cotización y genera un string tipo reporte
     * con estas variables
     *
     * @return string cotización
     */
    public String cotizar() {
        String resutaldo = "Cliente: %s\n\n"
                + "Área Paredes:  %.2f\n"
                + "Área Ventanas: %.2f\n"
                + "Área a Pintar: %.2f\n\n"
                + "Tiempo Aproximado: $%d dias y %dhrs\n"
                + "Costo: $%d";
        double tPintar = tParedes - tVentanas;
        double tiempo = tPintar * MIN_X_M2;
        int hrs = (int) Math.round(tiempo / 60 + 0.5);
        int costo = hrs * COSTO_X_HORA;
        return String.format(resutaldo, cliente, tParedes, tVentanas,
                tPintar, hrs / 24, hrs % 24, costo);
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getCliente() {
        return cliente;
    }

}
