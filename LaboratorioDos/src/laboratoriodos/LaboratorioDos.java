/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriodos;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class LaboratorioDos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String titulo = "Pintor UTN - v0.1";
        String menu1 = "1. Nueva cotización\n"
                + "2. Salir";

        String menu2 = "Desea agregar otra pared: \n"
                + "1. SI\n"
                + "2. NO";

        String menu3 = "Desea agregar una u otra ventana a la pared #%d \n"
                + "1. SI\n"
                + "2. NO";

        String menu4 = "Tipo de la ventana #%d\n"
                + "1. Rectangular/Cuadrada\n"
                + "2. Circular";

        while (true) {
            int op = Util.leerIntJOP(menu1, titulo);
            if (op == 2) {
                Util.mostrarJOP("Gracias por utilizar la aplicación!!");
                break;
            } else if (op == 1) {
                String cli = Util.leerStringJOP("Digite el nombre del cliente", titulo);
                PintorUTN cot = new PintorUTN(cli);
                int canParedes = 1;
                while (true) {
                    double ancho = Util.leerDoublesJOP("Digite el ancho de la pared #" + canParedes, titulo);
                    double largo = Util.leerDoublesJOP("Digite el alto de la pared #" + canParedes, titulo);
                    cot.agregarPared(ancho, largo);
                    int canVentanas = 1;
                    while (true) {
                        op = Util.leerIntJOP(String.format(menu3, canParedes), titulo);
                        if (op == 2) {
                            break;
                        } else if (op == 1) {
                            op = Util.leerIntJOP(String.format(menu4, canVentanas), titulo);
                            if (op == 1) {
                                ancho = Util.leerDoublesJOP("Digite el ancho de la ventana #" + canVentanas, titulo);
                                largo = Util.leerDoublesJOP("Digite el alto de la ventana #" + canVentanas, titulo);
                                cot.agregarVentana(ancho, largo);
                            } else {
                                ancho = Util.leerDoublesJOP("Digite el ancho(diametro) de la ventana #" + canVentanas, titulo);
                                cot.agregarVentana(ancho);
                            }
                            canVentanas++;
                        }
                    }
                    op = Util.leerIntJOP(menu2, titulo);
                    if (op == 2) {
                        break;
                    }
                    canParedes++;
                }
                Util.mostrarJOP(cot.cotizar());
            }
        }
    }
}
