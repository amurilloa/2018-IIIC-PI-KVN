/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class Temperatura {

    private double gradosCel;

    public Temperatura() {
    }

    public Temperatura(double gradosCel) {
        this.gradosCel = gradosCel;
    }

    public double convertirAFarenheit() {
        return gradosCel * 9 / 5 + 32;
    }

    public double getGradosCel() {
        return gradosCel;
    }

    public void setGradosCel(double gradosCel) {
        this.gradosCel = gradosCel;
    }
}
