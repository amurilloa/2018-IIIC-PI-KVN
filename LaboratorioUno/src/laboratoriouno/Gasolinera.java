/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class Gasolinera {

    private double galones;
    private double precio;

    private final double LIT_X_GAL;

    public Gasolinera(double precio) {
        this.precio = precio;
        LIT_X_GAL = 3.78541;
    }

    public double calcularCobro() {
        return galones * LIT_X_GAL * precio;
    }

    public double getGalones() {
        return galones;
    }

    public void setGalones(double galones) {
        this.galones = galones;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }    
}
