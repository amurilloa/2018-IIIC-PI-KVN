/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class Articulo {

    private int clave;
    private String descripcion;
    private double precio;
    private int cantidad;

    public Articulo(int clave, String descripcion, double precio, int cantidad) {
        this.clave = clave;
        this.descripcion = descripcion;
        this.precio = precio;
        this.cantidad = cantidad;
    }

    /**
     * Calcula el impuesto de valor agregado de todos los articulos
     *
     * @return double IVA
     */
    public double calcularIVA() {
        return precio * cantidad * 0.13;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

}
