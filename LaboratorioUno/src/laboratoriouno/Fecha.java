/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class Fecha {

    private int dia;
    private int mes;
    private int anno;

    public Fecha() {
        dia = 1;
        mes = 1;
        anno = 2000;
    }

    public Fecha(int dia, int mes, int anno) {
        if (verificar(dia, mes, anno)) {
            this.dia = dia;
            this.mes = mes;
            this.anno = anno;
        } else {
            this.dia = 1;
            this.mes = 1;
            this.anno = 2000;
        }
    }

    /**
     * Asigna una fecha válida
     *
     * @param dia int dia
     * @param mes int mes
     * @param anno int año
     */
    public void setFecha(int dia, int mes, int anno) {
        if (verificar(dia, mes, anno)) {
            this.dia = dia;
            this.mes = mes;
            this.anno = anno;
        } else {
            this.dia = 1;
            this.mes = 1;
            this.anno = 2000;
        }
    }

    /**
     * Obtiene una fecha en formato DD/MM/YYYY
     *
     * @return String con la fecha
     */
    public String getFecha() {
        String f = String.format("%02d/%02d/%04d", dia, mes, anno);
        return f;
    }

    /**
     * Obtiene una fecha en formato largo
     *
     * @return String con la fecha
     */
    public String getFechaLetras() {
        String f = String.format("%02d de %s de %04d",
                dia,
                convertir(mes),
                anno);
        return f;
    }

    /**
     * Convierte un número a su mes en letras correspondiente
     *
     * @param mes int mes (ej 1)
     * @return String mes (ej enero)
     */
    private String convertir(int mes) {
        switch (mes) {
            case 1:
                return "enero";
            case 2:
                return "febrero";
            case 3:
                return "marzo";
            case 4:
                return "abril";
            case 5:
                return "mayo";
            case 6:
                return "junio";
            case 7:
                return "julio";
            case 8:
                return "agosto";
            case 9:
                return "septiembre";
            case 10:
                return "octubre";
            case 11:
                return "noviembre";
            default:
                return "diciembre";
        }
    }

    /**
     * Determina si una fecha es válida
     *
     * @param dia int dia
     * @param mes int mes
     * @param anno int año
     * @return boolean true cuando la fecha es válida
     */
    private boolean verificar(int dia, int mes, int anno) {
        if (dia < 1 || dia > 31 || mes < 1 || mes > 12) {
            return false;
        } else if (dia > 30 && (mes == 4 || mes == 6 || mes == 9 || mes == 11)) {
            return false;
        } else if (mes == 2 && !bisiesto(anno) && dia > 28) {
            return false;
        } else if (mes == 2 && dia > 29) {
            return false;
        }
        return true;
    }

    /**
     * Determina sin un año es bisiesto
     *
     * @param anno int año
     * @return boolean true si el año es bisiesto
     */
    private boolean bisiesto(int anno) {
        if (anno % 4 == 0) {
            if (anno % 100 == 0) {
                return anno % 400 == 0;
            }
            return true;
        } else {
            return false;
        }
    }
}
