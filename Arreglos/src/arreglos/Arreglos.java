/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author ALLAN
 */
public class Arreglos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        int[] numeros = new int[10];
//        numeros[0] = 10;
//        System.out.println(numeros[0]);
//        System.out.println("Largo: " + numeros.length);
//        String[] dias = {"Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"};
//        System.out.println("Largo " + dias.length);
//        for (int i = 0; i < dias.length; i++) {
//            System.out.println(dias[i]);
//        }
//
//       
//        
//        Logica log = new Logica();
//        log.cargarPerros();
//        log.registrarPerro(new Perro(123, "Santa", true));
//        log.salvarPerros();
//
//        Perro t = new Perro(125, "Firulais", false);
//        log.registrarPerro(t);
//        System.out.println(log.imprimir());
//        int id = 125;
//        String nom = "Firulais";
//
//        Perro logeado = log.buscarPerro(id, nom);
//        if (logeado == null) {
//            System.out.println("Datos inválidos");
//        } else if (logeado.isPedigree()) {
//            System.out.println("Admin");
//        } else {
//            System.out.println("Perrillo");
//        }
//
//        String menu = "Menu\n"
//                + "Iniciar Sesion\n"
//                + "Registrarse\n"
//                + "Salir";

//        for (int i = 0; i < perros.length; i++) {
//            System.out.println(perros[i] != null ? perros[i].getNombre() : "Disponible");
//        }
//        for (Perro perro : perros) {
//            if (perro != null) {
//                System.out.println(perro.getNombre());
//            }
//        }
        int[] temp = {2, 3, 5, 6, 1, 7, 8, 2, 4, 6};
        System.out.println("Largo: " + temp.length);
        Practica p = new Practica(temp);
        System.out.println(p.imprimir());
        p.rotar();
        System.out.println(p.imprimir());
        p.rotar();
        System.out.println(p.imprimir());
        p.rotar();
        System.out.println(p.imprimir());
        p.rotar();
        System.out.println(p.imprimir());
        System.out.println("Promedio: " + p.promedio());
        System.out.println("Mayor: " + p.mayor());
        System.out.println("Suma Impares: " + p.sumarImp());
        int elemento = 5;
        System.out.println("Buscar: " + (p.buscar(elemento) ? "Si se encuentra en el arreglo"
                : "No se encuentra en el arreglo"));
    }

}
