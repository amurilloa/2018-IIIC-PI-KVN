/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Perro[] perros;

    public Logica() {
        perros = new Perro[25];
    }

    public void registrarPerro(Perro p) {
        for (int i = 0; i < perros.length; i++) {
            if (perros[i] == null) {
                perros[i] = p;
                break;
            }
        }
    }

    public String imprimir() {
        String txt = "";
        for (int i = 0; i < perros.length; i++) {
            if (perros[i] != null) {
                txt += perros[i].info() + "\n";
            }
        }
        return txt;

    }

    public Perro buscarPerro(int id, String nom) {
        for (Perro perro : perros) {
            if (perro != null && perro.getId() == id && perro.getNombre().equals(nom)) {
                return perro;
            }
        }
        return null;
    }

    public void salvarPerros() {
        //TODO: Buscar como guardar en un archivo
    }

    public void cargarPerros() {
        //TODO: Leer los perros del archivo perros.txt y meterlos en el arreglo de perros
    }

}
