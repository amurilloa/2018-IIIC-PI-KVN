/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author ALLAN
 */
public class Perro {

    private int id;
    private String nombre;
    private boolean pedigree;

    public Perro() {
    }

    public Perro(int id, String nombre, boolean pedigree) {
        this.id = id;
        this.nombre = nombre;
        this.pedigree = pedigree;
    }

    public String info() {
        return String.format("%d - %s",id, nombre);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isPedigree() {
        return pedigree;
    }

    public void setPedigree(boolean pedigree) {
        this.pedigree = pedigree;
    }

}
