/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author ALLAN
 */
public class Practica {

    private int[] arreglo;

    public Practica(int[] arreglo) {
        this.arreglo = arreglo;
    }

    public double promedio() {
        int total = 0;
        for (int i = 0; i < arreglo.length; i++) {
            total += arreglo[i];
        }
//        for (int num : arreglo) {
//            total += num;
//        }
        return (double) total / arreglo.length;
    }

    public int mayor() {
        int mayor = 0;
        if (arreglo.length > 0) {
            mayor = arreglo[0];
            for (int i = 0; i < arreglo.length; i++) {
                if (arreglo[i] > mayor) {
                    mayor = arreglo[i];
                }
            }
        }
        return mayor;
    }

    public int sumarImp() {
        int total = 0;
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] % 2 != 0) {
                total += arreglo[i];
            }
        }
        return total;
    }

    public boolean buscar(int elemento) {
        for (int i : arreglo) {
            if (i == elemento) {
                return true;
            }
        }
        return false;
    }

    /**
     * Busca un elemento dentro de un arreglo y retorna su posición
     *
     * @param elemento int elemento a buscar
     * @return int con la posición del elemento o -1 en caso de no encontrarlo
     */
    public int buscarPos(int elemento) {
        for (int i = 0; i < arreglo.length; i++) {
            if (elemento == arreglo[i]) {
                return i;
            }
        }
        return -1;
    }

    public void invertir() {
        int[] nuevo = new int[arreglo.length];
        int pos = 0;
        for (int i = arreglo.length - 1; i >= 0; i--) {
            nuevo[pos++] = arreglo[i];
        }
        arreglo = nuevo;
    }

    public void rotar() {

        int ult = arreglo[arreglo.length - 1];
        for (int i = arreglo.length - 1; i > 0; i--) {
            arreglo[i] = arreglo[i - 1];
        }
        arreglo[0] = ult;
    }

    public String imprimir() {
        String txt = "";
        for (int i = 0; i < arreglo.length; i++) {
            txt += arreglo[i] + ", ";
        }
        return txt + "\b\b";

    }

}
