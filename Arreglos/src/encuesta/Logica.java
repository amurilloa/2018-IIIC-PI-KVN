/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encuesta;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Formulario[] encuesta;

    public Logica() {
        encuesta = new Formulario[50];
    }

    public void registrar(Formulario f) {
        for (int i = 0; i < encuesta.length; i++) {
            if (encuesta[i] == null) {
                encuesta[i] = f;
                break;
            }
        }
    }

    public double porcentajeH() {
        int tp = 0;
        int th = 0;
        for (Formulario form : encuesta) {
            if (form != null) {
                tp++;
                if (form.getSexo() == 1) {
                    th++;
                }
            }
        }
        return th * 100 / tp;
    }

    public double porcentaje(int sexo) {
        int tp = 0;
        int th = 0;
        for (Formulario form : encuesta) {
            if (form != null) {
                tp++;
                if (form.getSexo() == sexo) {
                    th++;
                }
            }
        }
        return th * 100.0 / tp;
    }

    public double porcentajeTrab(int sexo) {
        int tp = 0;
        int th = 0;
        for (Formulario form : encuesta) {
            if (form != null) {
                if (form.getSexo() == sexo) {
                    tp++;
                    if (form.getTrabaja() == 1) {
                        th++;
                    }
                }
            }
        }
        return th * 100.0 / tp;
    }

    public double promedio(int sexo) {
        int tp = 0;
        int sueldo = 0;
        for (Formulario form : encuesta) {
            if (form != null) {
                if (form.getSexo() == sexo) {
                    if (form.getTrabaja() == 1) {
                        tp++;
                        sueldo += form.getSueldo();
                    }
                }
            }
        }
        return sueldo / tp;
    }
}
