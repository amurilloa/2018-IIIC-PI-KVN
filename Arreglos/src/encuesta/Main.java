/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encuesta;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();

        log.registrar(new Formulario(1, 1, 1, 234));
        log.registrar(new Formulario(1, 1, 0, 0));
        log.registrar(new Formulario(1, 1, 0, 0));
        log.registrar(new Formulario(8, 1, 1, 350));
        log.registrar(new Formulario(5, 1, 1, 550));
        log.registrar(new Formulario(6, 1, 1, 320));

        log.registrar(new Formulario(3, 2, 1, 350));
        log.registrar(new Formulario(4, 2, 0, 0));
        log.registrar(new Formulario(7, 2, 0, 0));
        log.registrar(new Formulario(9, 2, 1, 353));
        log.registrar(new Formulario(2, 2, 1, 450));

        String str = String.format("Hombres: %.0f%%", log.porcentaje(1));
        System.out.println(str);
        str = String.format("Mujeres: %.0f%%", log.porcentaje(2));
        System.out.println(str);
        str = String.format("Hombres Trab.: %.0f%%", log.porcentajeTrab(1));
        System.out.println(str);
        str = String.format("Mujeres Trab.: %.0f%%", log.porcentajeTrab(2));
        System.out.println(str);
        str = String.format("Sueldo Prom de Hombres.: %.0f", log.promedio(1));
        System.out.println(str);
        str = String.format("Sueldo Prom de Mujeres.: %.0f", log.promedio(2));
        System.out.println(str);

    }
}
