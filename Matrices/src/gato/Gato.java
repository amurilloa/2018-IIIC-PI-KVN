/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

/**
 *
 * @author ALLAN
 */
public class Gato {

    private char[][] tablero;
    private boolean turno;

    public Gato() {
        tablero = new char[3][3];
        turno = true;
    }

//    public void llenarAsientos() {
//        for (int f = 0; f < tablero.length; f++) {
//            for (int c = 0; c < tablero[f].length; c++) {
//                String nombre = String.valueOf(((char) (65 + f))) + (c + 1);
//                System.out.println(nombre);
//                tablero[f][c] = new Asiento(nombre, precio, true);
//            }
//        }
//
//    }
    public String verTablero() {
        String str = "";
        for (int f = 0; f < tablero.length; f++) {
            for (int c = 0; c < tablero[f].length; c++) {
                str += (tablero[f][c] == '\u0000' ? '_' : tablero[f][c]) + " ";
            }
            str += "\n";
        }
        return str;
    }

    public boolean marcar(int f, int c) {
        if (tablero[f - 1][c - 1] == '\u0000') {
            tablero[f - 1][c - 1] = turno ? 'X' : 'O';
            turno = !turno;
            return true;
        }
        return false;
    }

    public boolean gano() {
        for (int f = 0; f < tablero.length; f++) {
            if (tablero[f][0] != '\u0000' && tablero[f][0] == tablero[f][1] && tablero[f][0] == tablero[f][2]) {
                return true;
            }
        }
        for (int c = 0; c < tablero[0].length; c++) {
            if (tablero[0][c] != '\u0000' && tablero[0][c] == tablero[1][c] && tablero[0][c] == tablero[2][c]) {
                return true;
            }
        }
        if (tablero[0][0] != '\u0000' && tablero[0][0] == tablero[1][1] && tablero[0][0] == tablero[2][2]) {
            return true;
        }
        if (tablero[0][2] != '\u0000' && tablero[0][2] == tablero[1][1] && tablero[0][2] == tablero[2][0]) {
            return true;
        }
        return false;
    }

    public boolean empato() {
        for (int f = 0; f < tablero.length; f++) {
            for (int c = 0; c < tablero[f].length; c++) {
                if (tablero[f][c] == '\u0000') {
                    return false;
                }
            }
        }
        return true;
    }

    public String getGanador() {
        return "gana el jugador con la marca " + (turno ? 'O' : 'X');
    }

}
