/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrices;

/**
 *
 * @author ALLAN
 */
public class Matrices {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logica log = new Logica(3, 3);
        System.out.println(log.imprimir());
        log.llenar(5);
        System.out.println(log.imprimir());
        System.out.println("Suma de Pares: " + log.sumarP());
        System.out.println("Promedio: " + log.promedio());
        
        //Clase lógica: Tiene como atributo una matriz 
        //Contructtor que recibe por parámetro filas y columnas
        //Hacer un método de imprimir 
        //Hacer un método llenarMatriz, que llena la matriz con números aleatorios
        //Suma los números pares de la matriz
        //Promedio de los números de la matriz
    }

}
