/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrices;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private int[][] matriz;

    public Logica(int fila, int columna) {
        matriz = new int[fila][columna];
    }

    public String imprimir() {
        String str = "";
        for (int fila = 0; fila < matriz.length; fila++) {
            for (int columna = 0; columna < matriz[fila].length; columna++) {
                str += matriz[fila][columna] + ", ";
            }
            str += "\b\b\n";
        }
        return str;
    }

    public void llenar(int max) {
        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                matriz[f][c] = (int) (Math.random() * max) + 1;
            }
        }
    }

    public int sumarP() {
        int total = 0;
        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                if (matriz[f][c] % 2 == 0) {
                    total += matriz[f][c];
                }
            }
        }
        return total;
    }

    public double promedio() {
        double total = 0;
        int cant = 0;
        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                total += matriz[f][c];
                cant++;
            }
        }
        return total/cant;
    }

}
