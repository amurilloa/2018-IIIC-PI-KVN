/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecameniallanmurillo;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class EcamenIAllanMurillo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logica log = new Logica();
        int[] arreglo = {12, 3, 43, 6, 5, 3, 7, 8, 9, 4};
        System.out.println(log.imprimir(arreglo));

        String menu = "Menu Principal\n"
                + "i.   Imprimir Números\n"
                + "ii.  Sumar Pares\n"
                + "iii. Cantidad de Primos\n"
                + "iv.  Salir\n"
                + "Seleccione una opción: ";

        while (true) {
            String op = Util.leerString(menu);
            if (op.equals("i")) {
                arreglo = log.crearArreglo(5, 10);
                System.out.println(log.imprimir(arreglo));
                arreglo = log.crearArreglo(10, 1);
                System.out.println(log.imprimir(arreglo));
            } else if (op.equals("ii")) {
                int can = log.sumarPares(3, arreglo);
                System.out.println("Suma de Pares: " + can);
            } else if (op.equals("iii")) {
                int can = log.contarPrimos(arreglo);
                System.out.println("Cantidad de Primos: " + can);
            } else if (op.equals("iv")) {
                break;
            }
        }

        Estudiante[] estudiantes = new Estudiante[10];
        estudiantes[0] = new Estudiante(1, "Allan", "Murillo", "Alfaro", "amurillo@utn.ac.cr", "8526-2638");
        estudiantes[1] = new Estudiante(2, "Roberto", "Murillo", "Alfaro", "rmurillo@utn.ac.cr", "7526-2638");
        estudiantes[2] = new Estudiante(3, "Juan", "Murillo", "Alfaro", "jmurillo@utn.ac.cr", "8626-2638");

        for (Estudiante estudiante : estudiantes) {
            if (estudiante != null) {
                System.out.println(estudiante.obtenerNombre());
            }
        }

    }

}
