/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecameniallanmurillo;

/**
 *
 * @author ALLAN
 */
public class Logica {

    public String imprimir(int[] arreglo) {
        String str = "";
        for (int i = 0; i < arreglo.length; i++) {
            str += arreglo[i] + ", ";
        }
        return str + "\b\b";
    }

    public int[] crearArreglo(int inicio, int fin) {
        int[] temp = new int[Math.abs(fin - inicio) + 1];
        if (inicio < fin) {
            for (int i = 0; i < temp.length; i++) {
                temp[i] = inicio++;
            }
        } else {
            for (int i = 0; i < temp.length; i++) {
                temp[i] = inicio--;
            }
        }
        return temp;
    }

    public int sumarPares(int n, int[] arreglo) {
        int total = 0;
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] % 2 == 0) {
                total += arreglo[i];
                n--;
                if (n == 0) {
                    break;
                }
            }
        }
        return total;
    }

    public int contarPrimos(int[] arreglo) {
        int con = 0;
        for (int i = 0; i < arreglo.length; i++) {
            if (esPrimo(arreglo[i])) {
                con++;
            }
        }
        return con;
    }

    /**
     * Determina sin es primo o es compuesto
     *
     * @param numero
     * @return boolean true cuando es primo y false cuando es compuesto
     */
    public boolean esPrimo(int numero) {
        int div = 0;
        for (int i = 1; i <= numero; i++) {
            if (numero % i == 0) {
                div++;
            }
            if (div > 2) {
                return false;
            }
        }
        return div <= 2;
    }
}
