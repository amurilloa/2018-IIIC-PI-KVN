/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class Sintaxis {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        //Tipos de datos booleanos
//        boolean bandera = true;
//
//        //Tipos de datos númericos
//        byte num1 = 127;
//        int num2 = 1231232342;
//        long num3 = 123223423423L;
//        double num4 = 123.23;
//        float num5 = 123.23F;
//
//        //Tipos de datos Char 
//        char letra = 'A';
//        char digito = '5';
//
//        //Caracteres de escape
//        char escape = '\t';
//        System.out.println("Hola\tPerro");
//        String texto = "Text";
//
//        System.out.println("A\tB\tC\n1\t2\t3\n");
//        System.out.println("\"HOLA\"");
//        System.out.println("El caracter de escape \\n cambia de línea");
//        System.out.println("El unicode \\u0040 vale \u0040");
//        System.out.println("El unicode \\u00D1 vale \u00D1");
//        System.out.println("El unicode \\u221A vale \u221A");
//        System.out.println("\u221A144=12");
//
//        //Declaracion, Inicialización, Asignación
//        int a;
//        //Inicialización
//        a = 12;
//        System.out.println(a);
//        //Asignación
//        a = 13;
//        //Declarar e inicializar en la misma línea 
//        String a1 = "Hola";
//
//        String textoUno = "asda";
//        String hosRegPri = "Hospital de n.....";
//        String hosRegSec = "Hospital de n.....";
//        boolean esNumero = true;
//
//        //Los nombres de las variables ayudan a comprender el funcionamiento del
//        //codigo
//        String nom = "";
//        String apeUno = "";
//        String apeDos = "";
//        System.out.println(nom + " " + apeUno + " " + apeDos);
//        //Declaramos una sola variable por linea
//        double aproximacionDelValorPI = 3.14;
//        int z = a;
//        int x = z - num2;
//        int y = 10;
//        System.out.println(x);
//
//        double radio = 1.2;
//
//        //Tener cuidado con los nombres muuuuy significativos 
//        double aproximacionDelAreaDelCirculo = aproximacionDelValorPI
//                * Math.pow(radio, 2);
//
//        //Constante, siempre mayuscula e inicializada de una vez
//        final double VALOR_PI = 3.1415926;
//
//        boolean isReal = true;
//        byte b = 122;
//        short s = -29000;
//        int i = 100000;
//        long l = 999999999999L;
//        float f1 = 234.99F;
//        double d;
//
//        char cvalue = '4';
//        System.out.println(cvalue);
//        cvalue = 4;
//        System.out.println(cvalue);
//
//        i = (int) 32.99;
//        d = 32.99 - i;
//        System.out.println(i);
//        System.out.println(d);
//        b = (byte) 257;
//        System.out.println(b);

        //Operadores Aritmeticos
//        int x = 4;
//        double a = (double) 35 / x;
//        int b = 35 % 4;
//        System.out.println(a);
//        System.out.println(b);
//        b = b + 1;
//        b += 1;
//        b++;
//        
//        System.err.println("Allan");
//        System.out.println("Murillo");
//        System.err.println("Alfaro");
//        Entrada y salida de dato
        String menu = "Menu Principal\n"
                + "1. Opción 1\n"
                + "2. Opción 2\n"
                + "3. Salir\n"
                + "Seleccione una opción: ";

//        String opcion = JOptionPane.showInputDialog(menu);
//        JOptionPane.showMessageDialog(null, "Opción seleccionada es: " + opcion,
//                "Ventana de Prueba",
//                JOptionPane.QUESTION_MESSAGE);
//        JOptionPane.showMessageDialog(null, "Opción seleccionada es: " + opcion,
//                "Ventana de Prueba",
//                JOptionPane.WARNING_MESSAGE);
//        JOptionPane.showMessageDialog(null, "Opción seleccionada es: " + opcion,
//                "Ventana de Prueba",
//                JOptionPane.ERROR_MESSAGE);
//        JOptionPane.showMessageDialog(null, "Opción seleccionada es: " + opcion,
//                "Ventana de Prueba",
//                JOptionPane.PLAIN_MESSAGE);
        Logica log = new Logica();
        int res = log.sumar(31, 10);
        System.out.println(res);
        System.out.println(res * 3);
        res =  log.restar(31, 10);
        System.out.println(res);
        System.out.println(log.multiplicar(31, 10));
        System.out.println(log.dividir(31, 10));
        
        //Paso de parametro por valor
        int x = 10;
        System.out.println("Antes: "+ x);
        x = log.ejemploValor(x);
        System.out.println("Después: "+ x);
        
        //Paso de parametro por referencia
        System.out.println(log.nombre);
        log.ejemploReferencia(log);
        System.out.println(log.nombre);
        
        Casa c1 = new Casa();
        Casa c2 = new Casa();
        Casa c3 = new Casa();
        Casa c4 = new Casa();
        Casa c5 = new Casa();
        c1.color = "Blanca";
        
        c2.color = "Azul";
        
        System.out.println("C1-Color: "+ c1.color);
        System.out.println("C2-Color: "+ c2.color);
        
       
        
        
        
        
    }
}
