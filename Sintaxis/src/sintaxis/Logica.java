/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

/**
 *
 * @author ALLAN
 */
public class Logica {

    String nombre;
    
    //nivel acceso + tipo + nombre del metodo + (parametros)
    /**
     * Suma dos números enteros
     *
     * @param num1 int numero 1
     * @param num2 int numero 2
     * @return El resultado de sumar num1 + num2
     */
    public int sumar(int num1, int num2) {
        return num1 + num2;
    }

    /**
     * Resta dos números enteros
     *
     * @param num1 int numero 1
     * @param num2 int numero 2
     * @return El resultado de restar num1 - num2
     */
    public int restar(int num1, int num2) {
        return num1 - num2;
    }

    /**
     * Multiplica dos numeros enteros
     *
     * @param num1 int numero 1
     * @param num2 int numero 2
     * @return Resultado de la multiplicación de numero 1 * numero 2
     */
    public int multiplicar(int num1, int num2) {
        return num1 * num2;
    }

    /**
     * Divide dos numeros enteros
     *
     * @param num1 int numero 1
     * @param num2 int numero 2
     * @return Resultado de la división de numero 1 - numero 2
     */
    public double dividir(int num1, int num2) {
        return (double) num1 / num2;
    }
    
    public int ejemploValor(int x){
        x = x + 10;
        return x;
    }
    
    public void ejemploReferencia(Logica patito){
        patito.nombre = "Pedro";
    }
    
}
