/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expresionesbooleanas;

/**
 *
 * @author ALLAN
 */
public class Logica {

    public String dia(int dia) {
        String res = "";
        switch (dia) {
            case 1:
                res = "Domingo";
                break;
            case 2:
                res = "Lunes";
                break;
            case 3:
                res = "Martes";
                break;
            case 4:
                res = "Miércoles";
                break;
            case 5:
                res = "Jueves";
                break;
            case 6:
                res = "Viernes";
                break;
            case 7:
                res = "Sábado";
                break;
            default:
                res = "Día inválido";
        }
        return res;
    }
}
