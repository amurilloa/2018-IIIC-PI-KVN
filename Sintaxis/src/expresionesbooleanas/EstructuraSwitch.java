/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expresionesbooleanas;

/**
 *
 * @author ALLAN
 */
public class EstructuraSwitch {

    public static void main(String[] args) {
        int var = 0;
        
        switch (var) {
            case 1:
                System.out.println("Uno");
                break;
            case 2:
                System.out.println("Dos");
                System.out.println("Dos");
                break;
            case 3:
                System.out.println("Tres");
                System.out.println("Tres");
                System.out.println("Tres");
                break;
            default:
                System.out.println("Cualquier otro número");
        }

        int dia = 3;
        Logica log = new Logica();
        System.out.println(log.dia(dia));

    }
}
