/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class Principal {

    public static void main(String[] args) {

        Logica log = new Logica();

        String menu = "Calculadora UTN - v0.1\n"
                + "(+) Sumar\n"
                + "(-) Restar\n"
                + "(*) Multiplicar\n"
                + "(/) Dividir\n"
                + "(s) Salir\n"
                + "Seleccione una opción";

        APP:
        do {
            char op = Util.leerChar(menu);
            if (op == 's' || op == 'S') {
                System.out.println("Gracias por utilizar la aplicación");
                break;
            }
            int n1 = Util.leerInt("#1");
            int n2 = Util.leerInt("#2");
            switch (op) {
                case '+':
                    System.out.println("Sumar");
                    String res = String.format("%d+%d=%d", n1, n2, log.sumar(n1, n2));
                    System.out.println(res);
                    break;
                case '-':
                    System.out.println("Restar");
                    res = String.format("%d-%d=%d", n1, n2, log.restar(n1, n2));
                    System.out.println(res);
                    break;
                case '*':
                    System.out.println("Multiplicar");
                    res = String.format("%d*%d=%d", n1, n2, log.multiplicar(n1, n2));
                    System.out.println(res);
                    break;
                case '/':
                    System.out.println("Dividir");
                    res = String.format("%d/%d=%.2f", n1, n2, log.dividir(n1, n2));
                    System.out.println(res);
                    break;
//                case 's':
//                case 'S':
//                    System.out.println("Gracias por utilizar la aplicación");
//                    break APP;
                default:
                    System.out.println("Opción Inválida");
            }
        } while (true);
        System.out.println("Test");
    }
}
