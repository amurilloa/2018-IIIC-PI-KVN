/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

/**
 *
 * @author ALLAN
 */
public class Juego {

    private int aleatorio;
    private boolean gano;
    private int intentos;

    public Juego() {
        aleatorio = (int) (Math.random() * 20) + 1;
        intentos = 5;
    }

    public int getAleatorio() {
        return intentos > 0 ? 0 : aleatorio;
    }

    public boolean isGano() {
        return gano;
    }

    public int getIntentos() {
        return intentos;
    }

    public void jugar(int num) {
        gano = num == aleatorio;
        intentos--;
    }

    public boolean perdio() {
        return intentos == 0;
    }

    public String pista(int num) {
        return num > aleatorio ? "Es menor" : "Es mayor";
    }

}
