/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios;

import java.util.Scanner;
import util.Util;

/**
 *
 * @author ALLAN
 */
public class Practica {

    public static void main(String[] args) {
        Logica log = new Logica();
        Scanner sc = new Scanner(System.in);
        String menu = "Menu Principal\n"
                + "1. Ejercicio 1\n"
                + "2. Ejercicio 2\n"
                + "3. Ejercicio 3\n"
                + "4. Ejercicio 4\n"
                + "5. Ejercicio 5\n"
                + "6. Ejercicio 6\n"
                + "7. Salir\n"
                + "Seleccione una opción: ";

        System.out.print(menu);
        int op = Integer.parseInt(sc.nextLine());

        switch (op) {
            case 1:
                System.out.println("Ejercicio 1");
                int num = Util.leerInt("Digite un número: ");
                System.out.println(log.ejercicioUno(num));
                break;
            case 2:
                System.out.println("\nEjercicio 2\n" + log.ejercicioDos());
                break;
            case 3:
                System.out.println("\nEjercicio 3");
                int pa = Util.leerInt("Cant. Pasajeros: ");
                System.out.print("Num. Ejes: ");
                int ej = Integer.parseInt(sc.nextLine());
                System.out.print("Costo: ");
                int co = Integer.parseInt(sc.nextLine());
                double total = log.ejercicioTres(co, pa, ej);
                System.out.println("El costo total del vehículo es de: "
                        + total);
                break;
            case 4:
                System.out.println("\nEjercicio #4");
                System.out.print("Nota: ");
                double nota = Double.parseDouble(sc.nextLine());
                System.out.println("Rendimiento: " + log.ejercicioCuatro(nota));
                break;
            case 5:
                System.out.println("\nEjercicio 5");
                System.out.print("Canciones: ");
                int can = Integer.parseInt(sc.nextLine());
                System.out.print("Partituras: ");
                int par = Integer.parseInt(sc.nextLine());
                System.out.println("Nivel: " + log.ejercicioCinco(par, can));
                break;
            case 6:
                System.out.println("\nEjercicio #6");
                System.out.print("Monto: ");
                int monto = Integer.parseInt(sc.nextLine());
                System.out.println(log.ejercicioSeis(monto));
                break;
            case 7:
                System.out.println("Gracias por utilizar la aplicación");
                break;
            default:
                System.out.println("Opción Inválida");
        }
    }
}
